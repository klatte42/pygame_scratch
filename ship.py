import pygame

class Ship():
    def __init__(self, screen, settings):
        """initialize"""
        self.screen = screen
        self.image = pygame.image.load(settings.ship_image).convert_alpha()
        self.image = pygame.transform.scale(self.image, (settings.ship_width, settings.ship_height))
        self.rect = self.image.get_rect()
        self.screen_rect = self.screen.get_rect()
        self.speed = settings.ship_speed

        # start at bottom/center -- "center" is float for allowing non-integer speeds
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        self.center = float(self.rect.centerx)

        # settings for tracking moveement (when keypresses change)
        self.moving_right = False
        self.moving_left = False

    def update(self):
        """update location, based on movement flags -- if both right/left presesed, they offset to no movement
            -- must set rectangles centerx, based on floating "center" """
        if self.moving_left:
            self.center-= self.speed
        if self.moving_right:
            self.center += self.speed
        self.rect.centerx = self.center

        if self.rect.left < 0:
            self.rect.left = 0
            self.center = self.rect.centerx
        elif self.rect.right > self.screen_rect.right:
            self.rect.right = self.screen_rect.right
            self.center = self.rect.centerx


    def blitme(self):
        """draw ship"""
        self.screen.blit(self.image, self.rect)

    def get_size(self):
        return self.rect.width, self.rect.height

    def set_size(self, width=64, height=64):
        self.image = pygame.transform.scale(self.image, (width, height))
        self.rect = self.image.get_rect()

    def reset_location(self):
        """reset to starting location (like after resizing)"""
        # start at bottom/center
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom

