import sys, pygame
from time import sleep

from bullet import Bullet
from invader import Invader


def key_down_events(event, ship, screen, settings, bullets):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    elif event.key == pygame.K_LEFT:
        ship.moving_left = True
    elif event.key == pygame.K_q:
        sys.exit()
    elif event.key == pygame.K_SPACE:
        if len(bullets) < settings.bullet_max_count:
            new_bullet = Bullet(ship, screen, settings)
            bullets.add(new_bullet)


def key_up_events(event, ship, screen, settings, bullets):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    elif event.key == pygame.K_LEFT:
        ship.moving_left = False


def check_events(settings, screen, ship, bullets):
    """respond to keyboard and mouse events"""
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            key_down_events(event, ship, screen, settings, bullets)
        elif event.type == pygame.KEYUP:
            key_up_events(event, ship, screen, settings, bullets)


def update_bullets(bullets):
    """remove bullets that have flown past top of screen; call bullet.update() to move them"""
    for bullet in bullets.copy():
        if bullet.rect.bottom < 0:
            bullets.remove(bullet)
        else:
            bullet.update()


def update_invaders(invaders):
    invaders.update()


def do_collisions(settings, screen, ship, invaders, bullets):
    """processes collisions, deletes bullets and invaders that collide, returns bool as to whether player (ship) is dead)"""
    pygame.sprite.groupcollide(bullets, invaders, True, True)
    if len(invaders) == 0:
        settings.invader_speed += settings.invader_wave_speed_increase
        print("increasing invader speed to: " + str(settings.invader_speed))
        invaders.add(Invader(screen, settings))
    if pygame.sprite.spritecollideany(ship, invaders):
        print("ship hit!!!")
        settings.screen_bg_color=(settings.screen_bg_color[0], settings.screen_bg_color[1]-50, settings.screen_bg_color[2]-50)
        if settings.ship_lives > 1:
            settings.ship_lives -= 1
            print ("lives remaining: " + str(settings.ship_lives))
        else:
            print ("player died")
            return True
        settings.invader_speed += settings.invader_player_kill_speed_increase
        print("increasing invader speed to: " + str(settings.invader_speed))
        invaders.empty()
    return False


def update_screen(settings, screen, ship, invaders, bullets):
    screen.fill(settings.screen_bg_color)
    for invader in invaders:
        invader.blitme()
    for bullet in bullets:
        bullet.draw()
    #invader.blitme()
    ship.blitme()
    pygame.display.flip()


def end_game(settings, screen, ship):
    pygame.font.init()
    my_font = pygame.font.SysFont(None, 60)
    my_text_surface = my_font.render('YOU LOSE!!!', 1, (0,0,0))
    (x,y) = screen.get_rect().center
    x -= (my_text_surface.get_rect().width / 2)
    y -= (my_text_surface.get_rect().height / 2)
    
    screen.blit(my_text_surface, (x, y))
    pygame.display.update()
    sleep(2)

    