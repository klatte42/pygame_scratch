import sys, pygame
from pygame.sprite import Group

from settings import Settings
from ship import Ship
from invader import Invader
import game_functions

def run_game():
    pygame.init()

    my_settings = Settings()
    my_screen = pygame.display.set_mode( (my_settings.screen_width, my_settings.screen_height) )
    pygame.display.set_caption(my_settings.screen_caption)
    my_ship = Ship(my_screen, my_settings)
    my_bullets = Group()
    my_invaders = Group()
    my_invaders.add(Invader(my_screen, my_settings))

    player_dead = False
    while not player_dead:
        game_functions.check_events(my_settings, my_screen, my_ship, my_bullets)
        my_ship.update()
        #my_invaders.update()
        game_functions.update_invaders(my_invaders)
        game_functions.update_bullets(my_bullets)
        player_dead = game_functions.do_collisions(my_settings, my_screen, my_ship, my_invaders, my_bullets)
        game_functions.update_screen(my_settings, my_screen, my_ship, my_invaders, my_bullets)

    #if the player is dead, end the game
    game_functions.end_game(my_settings, my_screen, my_ship)
    exit()

run_game()