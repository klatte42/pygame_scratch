import pygame
from pygame.sprite import Sprite

class Invader(Sprite):
    def __init__(self, screen, settings):
        """ initialize and set location, based on ship location"""
        super().__init__()
        self.screen = screen
        self.image = pygame.image.load(settings.invader_image).convert_alpha()
        self.image = pygame.transform.scale(self.image, (settings.invader_width, settings.invader_height))
        self.rect = self.image.get_rect()
        self.screen_rect = self.screen.get_rect()
        self.speed = settings.invader_speed

        # start at top/left -- "self.x" is float for allowing non-integer speeds -- speed is on x-axis
        self.rect.x = 0
        self.rect.top = 0
        self.x = float(self.rect.x)

        # settings for tracking moveement
        self.moving_right = True
        self.moving_left = False

    def update(self):
        """update location, based on movement flags -- controlled entirely through hitting sides of screen
            -- must set rectangles x, based on floating "x" """
        if self.moving_left:
            self.x-= self.speed
        if self.moving_right:
            self.x += self.speed
        self.rect.x = self.x

        # if the invader goes past either side of the screen, keep inside screen, drop a level, and change direction
        if self.rect.left < 0:
            self.rect.left = 0
            self.x = self.rect.x
            self.moving_left = False
            self.moving_right = True
            self.rect.y += (self.rect.height +1)
        elif self.rect.right > self.screen_rect.right:
            self.rect.right = self.screen_rect.right
            self.x = self.rect.x
            self.moving_left = True
            self.moving_right = False
            self.rect.y += (self.rect.height +1)


    def blitme(self):
        """draw invader"""
        self.screen.blit(self.image, self.rect)
