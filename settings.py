class Settings():
    """settings for experimenting with pygame"""
    def __init__(self, width=1200, height=800, caption="my_game", background=(230, 230, 230)):
        #screen settings
        self.screen_width = width
        self.screen_height = height
        self.screen_bg_color = background
        self.screen_caption = caption

        # ship settings
        self.ship_width = 47
        self.ship_height = 64
        self.ship_speed = 1.5
        self.ship_image = "images/spaceship_cropped.png"
        self.ship_lives = 3

        # invader settings
        self.invader_width = 47
        self.invader_height = 64
        self.invader_speed = .5
        self.invader_wave_speed_increase = .1
        self.invader_player_kill_speed_increase = .5
        self.invader_image = "images/spaceship_cropped.png"

        # bullet settings
        self.bullet_speed = 1
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullet_max_count = 4
