import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    """bullets shot by player"""

    def __init__(self, ship, screen, settings):
        """ initialize and set location, based on ship location"""
        super().__init__()
        self.screen = screen

        # create at (0,0), then place at the top of the ship
        self.rect = pygame.Rect(0,0, settings.bullet_width, settings.bullet_height)
        self.rect.centerx = ship.rect.centerx
        self.rect.top = ship.rect.top

        # use float for y value, so we can have non-decimal speed
        self.y = float(self.rect.y)

        self.color = settings.bullet_color
        self.speed = settings.bullet_speed


    def update(self):
        """move up, based on speed, tracking float value for non-integer speed"""
        self.y -= self.speed
        self.rect.y = self.y

    def draw(self):
        pygame.draw.rect(self.screen, self.color, self.rect)